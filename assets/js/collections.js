/**
 * Library: eBooks Function Library
 *
 * @author Ralfe Poisson <ralfepoisson@gmail.com>
 * @copyright Copyright (c) Ralfe Poisson 2014
 */

/**
 * Get all Collections of a certain type from the server and
 * load them into the collections list.
 * @param type
 */
function getCollections(type) {
	// Log Activity
	Logger.Log(" * Retrieving Collections for type '" + type + "'");
	
    // Get Collections from Server
    var collections = Library.API.Collections.GetCollections(type);

    // Load Collections to List
    loadCollctions(collections, type);
}

/**
 * Retrieve a Collection from the server and load the items into the list
 * @param id
 */
function getCollection(id) {
	// Log Activity
	Logger.Log(" * Retrieving Collection #" + id);
	
    // Get Collection
    var collection = Library.API.Collections.GetCollection(id);
	Logger.Log(" - Collection: ", collection);

    // Load Items
    if (collection.type == "ebooks") {
        loadEbooks(collection.items);
		
		// Set the collection id in the new eBook form
		$("#newEbookCollectionId").val(id);
    }
    else if (collection.type == "books") {
    	loadBooks(collection.items);
    	
    	// Set the collection id in the new Book form
    	$("#newBookCollectionId").val(id);
    }
}

/**
 * Load eBooks into View
 * @param {Array} collections
 */
function loadCollctions(collections, type) {
    // Clear List
    $("#" + type + "_collections_list").html("");

    // Loop through Ebooks and add to the List
    for(var i = 0; i < collections.length; i++) {
        addCollectionToList(collections[i], type);
    }
}

/**
 * Add Collection to the View
 * @param {object} collection
 */
function addCollectionToList(collection, type) {
    // Get List
    var list = $("#" + type + "_collections_list");

    // Generate HTML
    var html = "<span class='collection_item btn btn-info' onclick='getCollection(" + collection.id + ")'>" + collection.name  + "</span>";

    // Add HTML to the List
    list.append(html);
}
