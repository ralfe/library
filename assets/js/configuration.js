/**
 * Library Configuration
 *
 * @author Ralfe Poisson <ralfepoisson@gmail.com>
 * @copyright Copyright (c) Ralfe Poisson 2014
 */

// Namespace
var Library = Library || {};

// Configuration
Library.Configuration = {
    Debug: true,
    APIServer : "./api/?"
};