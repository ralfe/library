/**
 * Library API Client
 *
 * @author Ralfe Poisson <ralfepoisson@gmail.com>
 * @copyright Copyright (c) Ralfe Poisson 2014
 */

// Namespace
var Library = Library || {};

// Create API Client
Library.API = new function() {
    var self = this;

    /**
     * Query the API Server and return the JSON object returned.
     * @param {string} action
     * @param {object} params
     * @returns {object}
     * @constructor
     */
    self.ApiCall = function(action, params){
        // Create URL
        var url = Library.Configuration.APIServer;
        url += "action=" + action;
        for (var key in params) {
            var value = params.hasOwnProperty(key) ? params[key] : "";
            url += "&" + key + "=" + value;
        }

        // Query Server
        var response = '';
        $.ajax({
            type: 'GET',
            url: url,
            async: false,
            success : function(text) {
                response = text;
            }
        });

        // Return Response
        return JSON.parse(response);
    };

    /**
     * Collections Endpoints
     */
    self.Collections = {
        AddCollection : function(name, type) {
            return self.ApiCall("add_collection", {name: name, type: type});
        },
        GetCollection : function(id) {
            return self.ApiCall("get_collection", {collection_id: id});
        },
        GetCollections : function(type) {
            return self.ApiCall("get_collections", {type: type});
        }
    };

    /**
     * eBooks EndPoints
     */
    self.eBooks = {
        AddeBook : function(title, collection_id, path) {
            return self.ApiCall("add_ebook", {title: title, collection_id: collection_id, file: path});
        },
        ScanPath: function(collection_id, path) {
            return self.ApiCall("ebooks_scan_path", {collection_id: collection_id, path: path});
        }
    };

    /**
     * Books EndPoints
     */
    self.Books = {
        AddBook : function(isbn, collection_id) {
            return self.ApiCall("add_book", {collection_id: collection_id, isbn: isbn});
        }
    };
};
