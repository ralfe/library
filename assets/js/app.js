/**
 * Library: Main Javascript Functions
 *
 * @author Ralfe Poisson <ralfepoisson@gmail.com>
 * @copyright Copyright (c) Ralfe Poisson 2014
 */

function loadType(type) {
	// Log Activity
	Logger.Log(" * Loading Type: '" + type + "'.");
	
    // Update Button Styles
    var elements = $(".type-button");
    for (var i = 0; i < elements.length; i++) {
        var e = elements[i];
        e.className = "type-button btn btn-info";
    }
    $("#type_" + type).attr("class", "type-button btn btn-primary");

    // Load the Collection
    getCollections(type);

    // Update Area
    elements = $(".area_active");
    for (var i = 0; i < elements.length; i++) {
        var e = elements[i];
        e.className = "area";
    }
    $("#area_" + type).attr("class", "area_active");
}

function view_details(item_id) {
    $(".item_details").each(function(item) {
        $(this).hide();
    });
    $("#item_details_" + item_id).show();
}

/**
 * Generate Item HTML
 * @param model
 */
function generate_item_html(model) {
        var details = "<span class='item_name'>" + model.name + "</span>";
        details += "<span class='item_description'>" + model.description.substring(0, 40) + "...</span>";
        details += (model.url.length > 0)? "<span class='item_link'><a href='" + model.url + "'>View more details</a></span>" : "";

        // Generate Book cover image
        var img = model.image.length > 0 ? "<img class='item' onclick='view_details(" + model.id + ")' src='" + model.image + "'>" : "<div class='item' onclick='view_details(" + model.id + ")'>&nbsp;</div>";

        // Generate Item HTML
        var html = "<div class='item_wrapper'>";
        html += "       " + img;
        html += "       <div class='item_details' id='item_details_" + model.id + "'>" + details + "</div>";
        html += "   </div>";

        // Return the HTML
        return html;
}