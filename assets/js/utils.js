/**
 * General Utils
 *
 * @author Ralfe Poisson <ralfepoisson@gmail.com>
 * @copyright Copyright (c) Ralfe Poisson 2014
 */

var Logger = new function() {
    var self = this;

    self.Log = function(message, obj) {
        if (Library.Configuration.Debug) {
        	if (typeof(obj) != 'undefined') {
            	console.log(message, obj);
            }
            else {
            	console.log(message);
            }
        }
    };
};
