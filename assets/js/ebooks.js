/**
 * Library: eBooks Function Library
 *
 * @author Ralfe Poisson <ralfepoisson@gmail.com>
 * @copyright Copyright (c) Ralfe Poisson 2014
 */

/**
 * Add an eBook on the Server
 */
function addEbook() {
    // Create Model
    var model = {
        name : $("#newEbookName").val(),
        collection_id : $("#newEbookCollectionId").val(),
        path: $("#newEbookPath").val()
    };

    // Log Activity
    Logger.Log("Adding eBook:", model);

    // Submit to Server
    var ebook = Library.API.eBooks.AddeBook(model.name, model.collection_id, model.path);

    // Add to the List
    addEbookToList(ebook);

    // Reset Form
    $("#newEbookName").val("");
    $("#newEbookPath").val("");
    
    // Hide Modal
    $("#newEbookModal").modal("hide");
}

/**
 * Load eBooks into View
 * @param {Array} ebooks
 */
function loadEbooks(ebooks) {
    // Clear List
    $("#ebooks_list").html("");

    // Loop through Ebooks and add to the List
    for(var i = 0; i < ebooks.length; i++) {
        addEbookToList(ebooks[i]);
    }
}

/**
 * Add eBook to the View
 * @param ebook
 */
function addEbookToList(ebook) {
    // Generate the HTML for the Item
    var model = {
        id: ebook.id,
        image: ebook.cover_image,
        name: ebook.title,
        description: ebook.description,
        url: ebook.url
    };
    var html = generate_item_html(model);

    // Add HTML to the List
    $("#ebooks_list").append(html);
}

/**
 * Scan directory for eBooks. This function is
 * triggered by a modal form submission.
 */
function scanForEbooks() {
    // Get Parameters
    var collection_id = $("#newEbookCollectionId").val();
    var path = encodeURI($("#ebookScanPath").val());

    // Submit to Server
    Library.API.eBooks.ScanPath(collection_id, path);

    // Reset Form
    $("#ebookScanPath").val("");

    // Hide Modal
    $("#ebookScanPathModal").modal("hide");
}