/**
 * Library: Books Function Library
 *
 * @author Ralfe Poisson <ralfepoisson@gmail.com>
 * @copyright Copyright (c) Ralfe Poisson 2014
 */

/**
 * Add an eBook on the Server
 */
function addBook() {
    // Create Model
    var model = {
        isbn : $("#newBookISBN").val(),
        collection_id : $("#newBookCollectionId").val()
    };

    // Log Activity
    Logger.Log("Adding Book:", model);

    // Submit to Server
    var book = Library.API.Books.AddBook(model.isbn, model.collection_id);

    // Add to the List
    addBookToList(book);

    // Reset Form
    $("#newBookISBN").val("");
    
    // Display Message
    $("#book_added_message").fadeOut("Slow", 
    	function() {
    		$("#book_added_message").fadeIn("Slow");
    		
    		// Give focus to the ISBN field
    		document.getElementById("newBookISBN").focus();
    	}
    );
}

/**
 * Load Books into View
 * @param {Array} books
 */
function loadBooks(books) {
    // Clear List
    $("#books_list").html("");

    // Loop through Ebooks and add to the List
    for(var i = 0; i < books.length; i++) {
        addBookToList(books[i]);
    }
}

/**
 * Add Book to the View
 * @param book
 */
function addBookToList(book) {
    // Generate the HTML for the Item
    var model = {
        id: book.id,
        image: book.cover_image,
        name: book.title,
        description: book.description,
        url: book.url
    };
    var html = generate_item_html(model);

    // Add HTML to the List
    $("#books_list").append(html);
}

/**
 * Triggered by Enter key being pressed in ISBN field.
 */
function process_new_isbn(event, e) {
	event.stopPropagation();
	var code = (event.keyCode ? event.keyCode : event.which);
	if (code == 13) {
		addBook();
	}
}

/**
 * Refreshes the listing of items
 */
function refresh_books() {
	// Get Current Collection ID
	var collection_id = $("#newBookCollectionId").val();
	
	// Reload the Collection
	getCollection(collection_id);
}


