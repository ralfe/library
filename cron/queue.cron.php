<?php
/**
 * Library: Queue Processing Cron Script
 * 
 * @author Ralfe Poisson <ralfepoisson@gmail.com>
 * @copyright Copyright (c) Ralfe Poisson 2014
 */

// ===============================================================
// SCRIPT SETUP
// ===============================================================

// Set the Application Directory
define("TINYMVC_APP_DIR", dirname(dirname(__FILE__)));

// Include the configuration for the application
require_once(dirname(dirname(__FILE__)) . "/config.php");

// Include the tinyMVC framework
require_once("tinyMVC/tinyMVC.php");

// Start the MVC
MVC::start(true);

// ===============================================================
// PROCESS
// ===============================================================

// Log Activity
logg(" [*] Queue Processing Cron Script starting up...");

// Get Queue Items for processing
$factory = new Queue();
$items = $factory->get("completion_date = 0000-00-00 00:00:00, retries < 4");
logg(" - Found " . sizeof($items) . " items for processing");

// Process the Items
foreach($items as $item) {
	// Log Activity
	logg(" - Processing {$item->type} #{$item->item_id}.");
	
	// Process Item according to its type
	if ($item->type == "ebook") {
		// Get the eBook
		logg("  > Retrieving eBook #{$item->item_id} ...");
		$ebook = new eBook($item->item_id);
		
		// Set Processing Start Time
		logg("  > Setting Processing timestamp");
		$item->processing_date = date("Y-m-d H:i:s");
		$item->save();
		
		// Fetch the Meta data for the book
		logg("  > Searching for Meta Data");
		$meta = BookHelper::getBookMetaData("", $ebook->title);
		
		// Check that we got meta data for the book
		if (!isset($meta->author)) { 
			logg("  > None found.");
			
			// Incrememt Retry Count
			$item->retries = $item->retries + 1;
			$item->save();
			continue; 
		}
		
		// Update the eBook
		logg("  > Updating the eBook with Meta Data");
		$ebook->cover_image = $meta->cover_image;
		$ebook->isbn = $meta->isbn;
		$ebook->description = (strlen($meta->description) > 5)? $meta->description : $ebook->description;
		$ebook->author = $meta->author;
		$ebook->year = $meta->year;
        $ebook->title = (strlen($ebook->title) < 1)? $meta->title : $ebook->title;
		logg(print_r($ebook->toObject(), true));
		
		// Set Queue Item as completed
		logg("  > Setting Completion timestamp");
		$item->completion_date = date("Y-m-d H:i:s");
		$item->save();
		
		// Save
		logg("  > Saving...");
		$ebook->save();
	}
	else if ($item->type == "book") {
		// Get the book
		logg("  > Retrieving eBook #{$item->item_id} ...");
		$book = new Book($item->item_id);
		
		// Set Processing Start Time
		logg("  > Setting Processing timestamp");
		$item->processing_date = date("Y-m-d H:i:s");
		$item->save();
		
		// Fetch the Meta data for the book
		logg("  > Searching for Meta Data");
		$meta = BookHelper::getBookMetaData($book->isbn);
		
		// Check that we got meta data for the book
		if (!isset($meta->author)) { 
			logg("  > None found.");
			
			// Incrememt Retry Count
			$item->retries = $item->retries + 1;
			$item->save();
			continue; 
		}
		
		// Update the Book
		logg("  > Updating the Book with Meta Data");
		$book->title = $meta->title;
		$book->cover_image = $meta->cover_image;
		$book->isbn = $meta->isbn;
		$book->description = (strlen($meta->description) > 5)? $meta->description : $ebook->meta_description;
		$book->author = $meta->author;
		$book->year = $meta->year;
		logg(print_r($book->toObject(), true));
		
		// Set Queue Item as completed
		logg("  > Setting Completion timestamp");
		$item->completion_date = date("Y-m-d H:i:s");
		$item->save();
		
		// Save
		logg("  > Saving...");
		$book->save();
	}
    else if ($item->type == "ebook_scan") {
        // Get the Scan Request
        logg("  > Retrieving scan request #{$item->item_id} ...");
        $request = new eBookScanRequest($item->item_id);

        // Set Processing Start Time
        logg("  > Setting Processing timestamp");
        $item->processing_date = date("Y-m-d H:i:s");
        $item->save();

        // Perform Scan
        logg("  > Performing the scan on '{$request->path}'.");
        $request->scan();

        // Set Queue Item as completed
        logg("  > Setting Completion timestamp");
        $item->completion_date = date("Y-m-d H:i:s");
        $item->save();

        // Done
        logg("  > Scan Complete.");
    }
}

logg(" - Done.");

// ===============================================================
// THE END
// ===============================================================

