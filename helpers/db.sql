CREATE DATABASE `library`;
USE `library`;

GRANT ALL PRIVILEGES ON `library`.* TO 'library'@'localhost' IDENTIFIED BY 'l1Br4R33';

CREATE TABLE `users` (
  `id` int(11) auto_increment,
  `username` varchar(255),
  `password` varchar(30),
  PRIMARY KEY (`id`)
);

CREATE TABLE `collections` (
  `id` int(11) auto_increment,
  `creation_date` datetime,
  `deactivation_date` datetime,

  `name` varchar(100),
  `type` varchar(30),

  PRIMARY KEY (`id`)
);

CREATE TABLE `ebooks` (
  `id` int(11) auto_increment,
  `collection_id` int(11),
  `creation_date` datetime,
  `deactivation_date` datetime,

  `title` varchar(255),
  `file` varchar(255),
  `cover_image` varchar(255),
  `favourite` int(1),

  `isbn` varchar(30),
  `description` text,
  `author` varchar(255),
  `url` varchar(255),

  PRIMARY KEY (`id`),
  FOREIGN KEY `collection_fk` (`collection_id`) REFERENCES `collections` (`id`)
);

CREATE TABLE `books` (
  `id` int(11) auto_increment,
  `collection_id` int(11),
  `creation_date` datetime,
  `deactivation_date` datetime,

  `title` varchar(255),
  `cover_image` varchar(255),
  `isbn` varchar(30),
  `description` text,
  `author` varchar(255),
  `url` varchar(255),
  `year` varchar(4),

  PRIMARY KEY (`id`)
);

CREATE TABLE `queue` (
  `id` int(11) auto_increment,
  `added_date` datetime,
  `processing_date` datetime,
  `completion_date` datetime,
  `collection_id` int(11),
  `item_id` int(11),
  `type` varchar(30),
  `retries` int(2) NOT NULL default 0,
  PRIMARY KEY(`id`),
  INDEX `date_fields` (`processing_date`, `completion_date`)
);

CREATE TABLE `ebook_scan_requests` (
  `id` int(11) auto_increment,
  `creation_date` datetime,
  `deactivation_date` datetime,
  `collection_id` int(11),
  `path` varchar(255),
  PRIMARY KEY(`id`)
);

INSERT INTO `collections`
 (`id`, `creation_date`, `type`, `name`)
VALUES
  (1, NOW(), 'ebooks', 'Unsorted'),
  (2, NOW(), 'books', 'Unsorted'),
  (3, NOW(), 'music', 'Unsorted'),
  (4, NOW(), 'movies', 'Unsorted'),
  (5, NOW(), 'series', 'Unsorted');
