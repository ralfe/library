<?php

class BookHelper {

    /**
     * Retrieves book meta data from internet sources
     * @param string $isbn
     * @param string $title
     * @return Book
     */
    public static function getBookMetaData($isbn="", $title="") {
    	// Log Activity
    	logg(" * Searching for Book metadata. ISBN: '{$isbn}', Title: '{$title}'.");

        // Validate Request
        if (strlen($isbn) == 0 && strlen($title) == 0) {
            logg(" - Aborting search for metadata as neither ISBN nor title was supplied.");
            return new Book();
        }

        // Compile URL
        $url = "https://www.googleapis.com/books/v1/volumes?";
        $url .= (strlen($isbn) > 0) ? "q=isbn:" . $isbn : "q=intitle:" . urlencode($title);
        $url .= "&key=" . "AIzaSyBfveBvzy8PgNeteI2rOo9ct0swTjBmsos";

        // Query the Books API
        $data = GeneralFunctions::get_json($url);
        
        // Return empty object if no meta data is found
        if (!isset($data->items)) { return null; }
        $info = $data->items[0]->volumeInfo;

        // Create a Book object to store the relevant meta data
        $meta = new Book();
        
        // Set the Meta Data
        $meta->isbn = $isbn;
        $meta->title = htmlentities($info->title);
        $meta->description = htmlentities($info->description);
        $meta->url = htmlentities($info->previewLink);
        $meta->year = htmlentities($info->publishedDate);
        $meta->author = htmlentities($info->authors[0]);
        $meta->author = (sizeof($info->authors) > 1)? $meta->author . " et al" : $meta->author;
        $meta->cover_image = (isset($info->imageLinks->thumbnail))? $info->imageLinks->thumbnail : BookHelper::tryGetBookCover($meta);

        // Return the Meta Data
        return $meta;
    }

    /**
     * Attempt to find the book cover
     * @param $book
     * @return string
     */
    public static function tryGetBookCover($book) {
        // Search Google
        $url = "https://www.googleapis.com/books/v1/volumes?q=intitle:" . urlencode($book->title);
        $url .= "&key=" . "AIzaSyBfveBvzy8PgNeteI2rOo9ct0swTjBmsos";

        // Query the Books API
        $data = GeneralFunctions::get_json($url);
        
        // Return empty object if no meta data is found
        if (!isset($data->items)) { return null; }
        
        // Get the first Image URL
        $total_items = sizeof($data->items);
        $cover_image = "";
        for ($i = 0; $i < $total_items; $i++){
        	$img = (isset($data->items[$i]->volumeInfo->imageLinks->thumbnail))? $data->items[$i]->volumeInfo->imageLinks->thumbnail : null;
        	if ($img != null) {
        		$cover_image = $img;
        		return $cover_image;
        	}
        }
        
        // If no image was found, return null
        return null;
    }

}
