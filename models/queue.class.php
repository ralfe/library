<?php
/**
 * Queue Class
 * @author Ralfe Poisson <ralfepoisson@gmail.com>
 * @version 1.0
 */
class Queue extends Model {

    // -----------------------------------------------------
    // Attributes
    // -----------------------------------------------------

    var $id;
    var $added_date;
    var $processing_date;
    var $completion_date;
    var $collection_id;
    var $item_id;
    var $type;

    // -----------------------------------------------------
    // Functions
    // -----------------------------------------------------

    /**
     * Constructor
     * @param int $id
     */
    public function __construct($id=0) {
        $this->table = "queue";
        $this->id = $id;
        $this->load();
    }

}
