<?php
/**
 * eBook Class
 * @author Ralfe Poisson <ralfepoisson@gmail.com>
 * @version 1.0
 */
class eBook extends Model {

    // -----------------------------------------------------
    // Attributes
    // -----------------------------------------------------

    var $id;
    var $collection_id;
    var $creation_date;
    var $deactivation_date;
    var $title;
    var $file;
    var $cover_image;
    var $favourite;
    var $isbn;
    var $description;
    var $author;
    var $url;

    // -----------------------------------------------------
    // Functions
    // -----------------------------------------------------

    /**
     * Constructor
     * @param int $id
     */
    public function __construct($id=0) {
        $this->table = "ebooks";
        $this->id = $id;
        $this->load();
    }

    /**
     * Return a simplified representation of this object to be
     * converted to JSON for use in the API.
     * @return stdClass
     */
    public function toObject() {
        // Create Flat Object
        $obj = new stdClass();
        $obj->id = $this->id;
        $obj->collection_id = $this->collection_id;
        $obj->title = $this->title;
        $obj->file = $this->file;
        $obj->cover_image = $this->cover_image;
        $obj->description = $this->description;
        $obj->url = $this->url;

        // Return Object
        return $obj;
    }

    /**
     * Add the item to the queue for processing. This involves
     * fetching meta data from the internet for the item.
     */
    public function addToQueue() {
        // Create new Queue Item
        $item = new Queue();
        $item->item_id = $this->id;
        $item->collection_id = $this->collection_id;
        $item->type = "ebook";
        $item->added_date = date("Y-m-d H:i:s");

        // Save
        $item->save();
    }

    /**
     * Derive the Title of the eBook from its file name.
     */
    public function derive_name_from_file() {
        $this->title = $this->file;

        // Remove the file extension
        $this->title = substr($this->title, 0, strrpos($this->title, "."));

        // Remove the Underscores
        $this->title = str_replace("_", " ", $this->title);

        // Remove the Directory
        $this->title = substr($this->title, strrpos($this->title, "\\") + 1);
    }
    
}
