<?php
/**
 * Book Class
 * @author Ralfe Poisson <ralfepoisson@gmail.com>
 * @version 1.0
 */
class Book extends Model {

    // -----------------------------------------------------
    // Attributes
    // -----------------------------------------------------

    var $id;
    var $collection_id;
    var $creation_date;
    var $deactivation_date;
    var $title;
    var $cover_image;
    var $isbn;
    var $description;
    var $author;
    var $url;
    var $year;

    // -----------------------------------------------------
    // Functions
    // -----------------------------------------------------

    /**
     * Constructor
     * @param int $id
     */
    public function __construct($id=0) {
        $this->table = "books";
        $this->id = $id;
        $this->load();
    }

    /**
     * Add the item to the queue for processing. This involves
     * fetching meta data from the internet for the item.
     */
    public function addToQueue() {
        // Create new Queue Item
        $item = new Queue();
        $item->item_id = $this->id;
        $item->collection_id = $this->collection_id;
        $item->type = "book";
        $item->added_date = date("Y-m-d H:i:s");

        // Save
        $item->save();
    }

    /**
     * Return a simplified representation of this object to be
     * converted to JSON for use in the API.
     * @return stdClass
     */
    public function toObject() {
        // Create Flat Object
        $obj = new stdClass();
        $obj->id = $this->id;
        $obj->collection_id = $this->collection_id;
        $obj->title = htmlentities($this->title);
        $obj->cover_image = $this->cover_image;
        $obj->description = htmlentities($this->description);
        $obj->url = $this->url;

        // Return Object
        return $obj;
    }

}
