<?php
/**
 * User Class
 * @author Ralfe Poisson <ralfepoisson@gmail.com>
 * @version 1.0
 */
class User extends Model {
    var $id;
    var $username;
    var $password;

    public function __construct($id=0) {
        $this->table = "users";
        $this->id = $id;
        $this->load();
    }
}
