<?php
/**
 * Collection Class
 * @author Ralfe Poisson <ralfepoisson@gmail.com>
 * @version 1.0
 */
class Collection extends Model {

    // -----------------------------------------------------
    // Attributes
    // -----------------------------------------------------

    var $id;
    var $creation_date;
    var $deactivation_date;
    var $name;
    var $type;

    var $items;

    // -----------------------------------------------------
    // Functions
    // -----------------------------------------------------

    /**
     * Constructor
     * @param int $id
     */
    public function __construct($id=0) {
        $this->table = "collections";
        $this->id = $id;
        $this->load();

        $this->items = Array();
    }

    /**
     * Populate the items array from the database.
     */
    public function load_items() {
        // Get Factory based on Type
        switch($this->type) {
            case "ebooks":
                $factory = new eBook();
                break;
            case "books";
                $factory = new Book();
                break;
            default:
                $factory = new eBook();
        }

        // Load Items
        $this->items = $factory->get("collection_id = {$this->id}");
    }

    /**
     * Return a simplified representation of this object for
     * JSON serialisation to be used in the API.
     * @return stdClass
     */
    public function toObject() {
        // Create Object
        $obj = new stdClass();
        $obj->id = $this->id;
        $obj->name = $this->name;
        $obj->type = $this->type;
        $obj->items = Array();

        // Load Items
        for($i = 0; $i < sizeof($this->items); $i++) {
            $obj->items[] = $this->items[$i]->toObject();
        }

        // Return Object
        return $obj;
    }
}
