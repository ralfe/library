<?php
/**
 * Library: eBook Scan Request model
 *
 * @author Ralfe Poisson <ralfepoisson@gmail.com>
 * @copyright Copyright (c) Ralfe Poisson 2014
 */

class eBookScanRequest extends Model {

    // -----------------------------------------------------
    // Attributes
    // -----------------------------------------------------

    var $id;
    var $collection_id;
    var $creation_date;
    var $deactivation_date;
    var $path;

    // -----------------------------------------------------
    // Functions
    // -----------------------------------------------------

    /**
     * Constructor
     * @param int $id
     */
    public function __construct($id=0) {
        $this->table = "ebook_scan_requests";
        $this->id = $id;
        $this->load();
    }

    /**
     * Return a simplified representation of this object to be
     * converted to JSON for use in the API.
     * @return stdClass
     */
    public function toObject() {
        // Create Flat Object
        $obj = new stdClass();
        $obj->id = $this->id;
        $obj->collection_id = $this->collection_id;
        $obj->path = $this->path;

        // Return Object
        return $obj;
    }

    /**
     * Add the item to the queue for processing. This involves
     * fetching meta data from the internet for the item.
     */
    public function addToQueue() {
        // Create new Queue Item
        $item = new Queue();
        $item->item_id = $this->id;
        $item->collection_id = $this->collection_id;
        $item->type = "ebook_scan";
        $item->added_date = date("Y-m-d H:i:s");

        // Save
        $item->save();
    }

    /**
     * Scan the path for potential eBooks to add to the
     * collection.
     */
    public function scan() {
        // Get Path to Scan
        $path = base64_decode($this->path);
        logg(" * Scanning '{$path}' ({$this->path}) for eBooks.");

        // Get all .pdf files recursively
        $directory = new RecursiveDirectoryIterator($path);
        $iterator = new RecursiveIteratorIterator($directory);
        $files = new RegexIterator($iterator, '/^.+\.pdf$/i', RecursiveRegexIterator::GET_MATCH);

        // Cycle through and add all the files
        foreach($files as $file) {
            // Create new eBook
            $ebook = new eBook();
            $ebook->file = $file[0];
            $ebook->creation_date = date("Y-m-d H:i:s");
            $ebook->collection_id = $this->collection_id;
            $ebook->derive_name_from_file();

            // Log Activity
            logg(" - {$ebook->title}");

            // Save
            $ebook->save();

            // Add to Queue
            $ebook->addToQueue();
        }
    }

}
