<?php

class Controller extends AbstractController {
	
	/**
	 * The default function called when the script loads
	 */
	function display(){
		# Create View
		$view_model = array();
		$view = new View("home/default.html", $view_model);
		
		# Display View
		$view->show();
	}

    function test(){
        $meta = BookHelper::getBookMetaData("9780808924326");
        var_dump($meta);
    }
}

