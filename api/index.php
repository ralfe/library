<?php
/**
 * Library API
 *
 * @author Ralfe Poisson <ralfepoisson@gmail.com>
 * @copyright Copyright Ralfe Poisson <ralfepoisson@gmail.com>
 * @version 1.0
 */

// ===============================================================
// SCRIPT SETUP
// ===============================================================

// Set the Application Directory
define("TINYMVC_APP_DIR", dirname(dirname(__FILE__)));

// Include the configuration for the application
require_once(dirname(dirname(__FILE__)) . "/config.php");

// Include the tinyMVC framework
require_once("tinyMVC/tinyMVC.php");

// Start the MVC
MVC::start(true);

// ===============================================================
// HANDLE REQUEST
// ===============================================================

// Check that an action is set
if (!isset($_GET['action']) || strlen($_GET['action']) == 0) {
	logg("API: Missing action.");
    print "{ Error: 'Missing Action' }";
}

// Get Action
$action = trim($_GET['action']);

// Check that the Action exists
if (!function_exists($action)) {
	logg("API: Invalid action '{$action}'");
    print "{ Error: 'Invalid Action' }";
}

// Handle Action
logg("API: Processing request '{$action}'.");
$result = $action($_REQUEST);

// Return JSON Encoded Result
print json_encode($result);

// ===============================================================
// FUNCTIONS
// ===============================================================

/**
 * Add a new eBook
 * @param {Array} $params
 * @return eBook
 */
function add_ebook($params){
	// Log Activity
	logg("API: Add eBook");
	
    // Create new eBook
    $ebook = new eBook();

    // Get Parameters
    $ebook->title = $params["title"];
    $ebook->file = $params["file"];
    $ebook->collection_id = ($params["collection_id"] > 0)? $params["collection_id"] : 2;
    logg("API: - Title: '{$ebook->title}'");
    logg("API: - File: '{$ebook->file}'");
    logg("API: - Collection: #{$ebook->collection_id}");

    // Set Computed Fields
    $ebook->creation_date = date("Y-m-d H:i:s");

    // Save Ebook
    $ebook->save();
    
    // Add to Queue for Processing
    $ebook->addToQueue();

    // Retrieve the eBook from the database
    $ebook = new eBook($ebook->id);

    // Return eBook
    return $ebook->toObject();
}

/**
 * Add a new Collection
 * @param {Array} $params
 * @return Collection
 */
function add_collection($params) {
	// Log Activity
	logg("API: Add Collection");
	
    // Create new Collection
    $collection = new Collection();

    // Get Parameters
    $collection->name = $params["name"];
    $collection->type = $params["type"];
    logg("API: - Name: {$collection->name}");
    logg("API: - Type: {$collection->type}");

    // Set Computed Fields
    $collection->creation_date = date("Y-m-d H:i:s");

    // Save Collection
    $collection->save();

    // Retrieve the Collection from the database
    $collection = new Collection($collection->id);

    // Return Collection
    return $collection->toObject();
}

/**
 * Get all active Collections
 * @param {Array} $params
 * @return array
 */
function get_collections($params) {
	// Log Activity
	logg("API: Get Collections");
	
    // Get Parameters
    $type = $params['type'];
    logg("API: - Type: {$type}");

    // Get Collections
    $factory = new Collection();
    $collections = $factory->get("type = {$type}, deactivation_date IS NULL");

    // Format Collection Items
    for ($i = 0; $i < sizeof($collections); $i++) {
        $collections[$i] = $collections[$i]->toObject();
    }

    // Return Collections
    return $collections;
}

/**
 * Get a Collection with all eBooks
 * @param {Array} $params
 * @return Collection
 */
function get_collection($params) {
	// Log Activity
	logg("API: Get Collection");
	
    // Get Collection
    $collection_id = $params["collection_id"];
    logg("API: - Collection ID: {$collection_id}");

    // Get Collection
    $collection = new Collection($collection_id);

    // Load Books
    $collection->load_items();

    // Return Collection
    return $collection->toObject();
}

/**
 * Add a new Book
 * @param {Array} $params
 * @return Book
 */
function add_book($params){
	// Log Activity
	logg("API: Add Book");
	
    // Create new eBook
    $book = new Book();

    // Get Parameters
    $book->isbn = $params["isbn"];
    $book->collection_id = ($params["collection_id"] > 0)? $params["collection_id"] : 2;
    logg("API: - ISBN: {$book->isbn}");
    logg("API: - Collection: #{$book->collection_id}");

    // Set Computed Fields
    $book->creation_date = date("Y-m-d H:i:s");

    // Save Ebook
    $book->save();

    // Add to Queue for Processing
    $book->addToQueue();

    // Retrieve the eBook from the database
    $book = new Book($book->id);

    // Return eBook
    return $book->toObject();
}

/**
 * Scan a local path for eBooks
 * @param {Array} $params
 */
function ebooks_scan_path($params) {
	// Log Activity
	logg("API: Scan Path");
	
    // Get Collection
    $collection_id = $params["collection_id"];
	
    // Get Path
    $path = $params["path"];
	logg("API: - Collection: #{$collection_id}");
	logg("API: - Path: {$path}");

    // Add Scan Request
    $request = new eBookScanRequest();
    $request->collection_id = $collection_id;
    $request->creation_date = date("Y-m-d H:i:s");
    $request->path = base64_encode($path);
    $request->save();

    // Add to Queue
    $request->addToQueue();
}

// ===============================================================
// THE END
// ===============================================================
